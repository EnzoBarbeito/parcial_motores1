using UnityEngine;
using UnityEngine.SceneManagement;
public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private Rigidbody rb;
    bool checksalto;
    public TMPro.TMP_Text TextoContador;
    public TMPro.TMP_Text TextoGameover;
    public TMPro.TMP_Text TextoPausa;
    public TMPro.TMP_Text TextoP2;
    public TMPro.TMP_Text TextoGameover2;
    public TMPro.TMP_Text TextoVidas;
    public TMPro.TMP_Text TextoEstado;
    public TMPro.TMP_Text TextoDa�o;
    public TMPro.TMP_Text TextoMonedas;
    public int vidas;
    private float tiempo;
    private float tiempoactual;
    string estado;
    public Camera CamaraJugador;
    public GameObject bala;
    private float monedas;
    bool estalento;
    bool ControlesInvertidos;
    bool pierde;
    bool gana;
    bool perfecto;
    public float Valor;
    void Start()
    {
        perfecto = false;
        pierde = true;
        gana = false;
        ControlesInvertidos = false;
        estalento = false;
        monedas = 0;
        estado="Normal";
        tiempo = 60;
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        tiempoactual=tiempo;
        inicializartextos();
    }

    private void Update()
    {
        respawn();
        reloj();
        inicializartextos();
        if (ControlesInvertidos == false){
            float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
            float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

            movimientoAdelanteAtras *= Time.deltaTime;
            movimientoCostados *= Time.deltaTime;
            transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        }

      
        if (EstaEnPiso())
        {
            checksalto = true;
        }
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (tiempoactual < 0)
        {
            gameover();

        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            pausar();

        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            continuar();

        }
        if (vidas <= 0)
        {
            gameover();
        }
        if (Input.GetMouseButtonDown(0)) //Poder disparar
        {
            Ray ray = CamaraJugador.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject bal;
            bal = Instantiate(bala, ray.origin, transform.rotation);

            Rigidbody rb = bal.GetComponent<Rigidbody>();
            rb.AddForce(CamaraJugador.transform.forward * 15, ForceMode.Impulse);

            Destroy(bal, 5);
        }
        if (Input.GetMouseButtonDown(1)) //Poder tocar
        {
            Ray ray = CamaraJugador.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;
            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                Debug.Log("El rayo toc� al objeto: " + hit.collider.name);

                if (hit.collider.name==("Boton")&& pierde==true)
                {
                    GameOverMeta(monedas);

                }
                else if (hit.collider.name == ("Boton") && gana== true)
                {
                    Ganaste9(monedas);

                }
                else if (hit.collider.name == ("Boton") && perfecto == true)
                {
                    Perfecto(monedas);
                }
            }

        }
        if (monedas >= 5)
        {
            pierde = false;
            gana = true;
        }
        if (monedas >= 10)
        {
            pierde = false;
            gana = false;
            perfecto = true;
        }
        if (ControlesInvertidos == true)
        {
            float movimientoAdelanteAtras = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
            float movimientoCostados = Input.GetAxis("Vertical") * rapidezDesplazamiento;
            movimientoAdelanteAtras *= Time.deltaTime;
            movimientoCostados *= Time.deltaTime;
            transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        }


        salto();
        dobsalto();
        hitdetection();
       
    }
   private void Ganaste9(float monedas)
   {
        if (monedas >= 5 || monedas < 10)
        {
            Time.timeScale = 0;
            estado = "Ganador!";
            TextoGameover.text = "VICTORIA";
            TextoGameover2.text = "R = Reintentar";
            Destroy(TextoMonedas);
            Destroy(TextoVidas);
            Destroy(TextoContador);
        }
    }
    private void GameOverMeta(float monedas)
    {
        if (monedas == 0 || monedas == 1 || monedas == 2 || monedas == 3 || monedas == 4)
        {
            Time.timeScale = 0;
            estado = "Perdedor!";
            TextoGameover.text = "GAME OVER";
            TextoGameover2.text = "Te faltan monedas. R = Reintentar";
            Destroy(TextoMonedas);
            Destroy(TextoVidas);
            Destroy(TextoContador);


        }
    }
    private void Perfecto(float monedas)
    {
        if (monedas <= 10)
        {
            Time.timeScale = 0;
            estado = "Ganador!";
            TextoGameover.text = "PERFECTO";
            TextoGameover2.text = "R = Reintentar";
            Destroy(TextoMonedas);
            Destroy(TextoVidas);
            Destroy(TextoContador);
        }
    }

    public void salto()
    {
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso() && checksalto)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }
    }
    public void dobsalto()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !EstaEnPiso() && checksalto)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            checksalto = false;
        }
    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coleccionable") == true)
        {
            other.gameObject.SetActive(false);
            rapidezDesplazamiento = rapidezDesplazamiento* 2;
            transform.localScale = transform.localScale * 5;
            estado = "Gigante";
        }
        if (other.gameObject.CompareTag("Coleccionable2") == true)
        {
            other.gameObject.SetActive(false);
            rapidezDesplazamiento = 10;
            transform.localScale = new Vector3(1f, 1f, 1f);
            estado = "Normal";
            estalento = false;
            ControlesInvertidos = false;
        }
        if (other.gameObject.CompareTag("Moneda") == true)
        {
            other.gameObject.SetActive(false);
            monedas+=1;

        }
       
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo") == true)
        {
            vidas--;
        }
        if (collision.gameObject.CompareTag("Enemigo Tipo4") == true && estalento == false)
        {
            rapidezDesplazamiento = rapidezDesplazamiento / 2;
            estalento = true;
            estado = "Ralentizado (Busca un Normalizador!)";
            if (ControlesInvertidos == true)
            {
                estado = "Lento e Invertido (Busca un Normalizador!)";

            }
          
        }
        if (collision.gameObject.CompareTag("Enemigo Controles") == true && ControlesInvertidos==false)
        {
            ControlesInvertidos = true;
            estado = "Invertido (Busca un Normalizador!)";
            if (ControlesInvertidos == true && estalento == true)
            {
                estado = "Invertido y Lento (Busca un Normalizador!)";

            }
        }
        if (collision.gameObject.CompareTag("Bala Enemiga") == true)
        {
            vidas--;
        }
        if (collision.gameObject.CompareTag("Chorro") == true && monedas>0)
        {
            monedas-= 1;
        }
    }
    void inicializartextos ()
    {
        TextoContador.text = "Tiempo restante: " + tiempoactual.ToString("0");
        TextoVidas.text = "Vidas : " + vidas.ToString();
        TextoEstado.text = "Estado: " + estado;
        TextoMonedas.text = "Monedas totales: " + monedas.ToString();

    }
    void pausar()
    {
        Time.timeScale = 0;
        TextoPausa.text = "PAUSA";
        TextoP2.text = "E = Continuar, R = Reiniciar";
    }
    void continuar()
    {
        Time.timeScale = 1;
        TextoPausa.text = "";
        TextoP2.text = "";

    }
    void reloj()
    {
        if (tiempoactual > 0)
        {
            tiempoactual -= Time.deltaTime;

        }
    }
    void gameover()
    {
        Time.timeScale = 0;
        TextoGameover.text = "GAME OVER";
        TextoContador.text = "";
        TextoGameover2.text = "Enter = Reiniciar";
        TextoP2.text = "";
        TextoVidas.text = "";
        Destroy(TextoMonedas);
        estado = "Muerto";
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Time.timeScale = 1;
        }

    }
    void respawn()
    {
        if (transform.position.y < -Valor)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
            }


        }
    }
    
    void hitdetection()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = CamaraJugador.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                Debug.Log("El rayo toc� al objeto: " + hit.collider.name);
            }
        }

    }


}
