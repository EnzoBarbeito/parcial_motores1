using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlEnemigo4 : MonoBehaviour
{
    public GameObject Jugador;
    public int rapidez;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
    
}
