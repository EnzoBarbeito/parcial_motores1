using UnityEngine;

public class ControlEnemigo3 : MonoBehaviour
{
    private int hp;
    public GameObject jugador;
    public int rapidez;

    void Start()
    {
        hp = 100;
    }

    private void Update()
    {
  
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    public void recibirDaņo()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            recibirDaņo();
        }

    }
}
