using UnityEngine;

public class Enemigo_Chorro : MonoBehaviour
{
    private int hp;
    public GameObject jugador;
    public int rapidez;


    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
}   
