using UnityEngine;

public class ScriptEnemigo2 : MonoBehaviour
{
    public GameObject jugador;
    public GameObject bala;
    bool pausa;
   
    private void Update()
    {
        Pausa();
        if (pausa==false)
        {
            transform.Rotate(new Vector3(0, 25, 0) * Time.deltaTime);
            transform.LookAt(jugador.transform);
            GameObject bal;
            bal = Instantiate(bala, jugador.transform.position, Quaternion.identity);
            Rigidbody rb = bal.GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 15, ForceMode.Impulse);

            Destroy(bal, 1.5f);
        }
    }
    private void Pausa()
    {
        if (Time.timeScale == 0)
        {
            pausa = true;
        }
        else if (Time.timeScale == 1)
        {
            pausa = false;
        }
    }

 
   
}